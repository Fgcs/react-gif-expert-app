import React, { useState } from "react";
import CategoryAdd from "./CategoryAdd";
import GifGrid from "./GifGrid";
// import PropTypes from "prop-types";

const GifExpertApp = () => {
  const [categories, setCategories] = useState(["One punch"]);
  // const handleAdd = () => {
  //   setCategories([...categories, "hunter x hunter"]);
  // };
  return (
    <>
      <h1>GifExpertApp</h1>
      <CategoryAdd setCategories={setCategories} />
      <hr />

      <ol>
        {categories.map((item) => (
          <GifGrid key={item} category={item} />
        ))}
      </ol>
    </>
  );
};

// GifExpertApp.propTypes = {};

export default GifExpertApp;
